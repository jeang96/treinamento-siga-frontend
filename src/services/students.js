import axios from "axios";

const apiUrl = "http://localhost:8080/aluno"

export async function getAllStudents() {
    const {data} = await axios.get(`${apiUrl}`);
    return data;
}

export async  function deleteStudentByMatricula(matricula) {
    const { data } = await axios.delete(`${apiUrl}/${matricula}`);
    console.log(data);
    return data;
}

export async function createStudent (params) {
    return await axios.post(`${apiUrl}`, params);
}

export async function editStudent (params) {
    const {data} = await axios.put(`${apiUrl}`, params);

    return data;
}

export async function cancelStudentRegistration (matricula) {
    try {
        return await axios.put(`${apiUrl}/cancelar/${matricula}`);
    }
     catch (error) {
        console.log(error);
        return error.response.data;
     }
}